#include <iostream>
#include <time.h>
using namespace std;
int main()
{
	const int N = 7;
	int array[N][N];
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
			cout << array[i][j] << ' ';
		}
		cout << endl;
	}
	cout << endl;

	struct tm buf;
	time_t t = time(NULL);
	localtime_s(&buf, &t);
	int day = buf.tm_mday;
	int sumElements = 0;
	for (int x = 0; x < N; x++)
	{
		sumElements += array[day % N][x];
	}
	cout << sumElements << endl;

	return 0;
}